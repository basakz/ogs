import json
import plotly.graph_objects as go

some_ctests = ["SurfaceComplexation", "EquilibriumPhase", "KineticReactant"]

for test in some_ctests:
	cTestFile = open(test + '.json', 'r') 

	print("Started reading nested JSON array")
	developerDict = json.load(cTestFile)
	#fileDict = {}
	Filelabel = []
	Filesource = []
	Filetarget = []
	Filevalue = []

	Filelabel.append("EquilibriumPhase")
	i = 0
	for file in developerDict["files"]:
		filecounter = 0
		for line in file['lines']: 
			#print(line['branches'])
			if line['branches']!= []:
				continue
			if line['count']!=0:
				filecounter += 1
				#print(line['count']) 
			#print(line['gcovr/noncode']) 
			#print(line['line_number']) 
		if filecounter !=0:
			#fileDict[file['file']] = filecounter
			Filelabel.append(file['file'])
			Filesource.append(0)
			i += 1
			Filetarget.append(i)
			Filevalue.append(filecounter)
			#print("Test files: ", file['file'])
			#print("File counter: ", filecounter)
	#print("Test lines: ", developerDict["files"]["lines"])
	#print(fileDict)
	print("Done reading nested JSON Array. Writing the Sankey Graph")

	fig = go.Figure(data=[go.Sankey(
		node = dict(
		  pad = 15,
		  thickness = 20,
		  line = dict(color = "black", width = 0.5),
		  label = Filelabel,
		  color = "blue"
		),
		link = dict(
		  source = Filesource, # indices correspond to labels, eg A1, A2, A1, B1, ...
		  target = Filetarget,
		  value = Filevalue
	  ))])

	fig.update_layout(title_text="Basic Sankey Diagram", font_size=10)
	fig.show()