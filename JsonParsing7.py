import json
import plotly.graph_objects as go

cTestFile = open('EquilibriumPhase.json', 'r') 

print("Started reading nested JSON array")
developerDict = json.load(cTestFile)
#fileDict = {}
Filelabel = []
Filesource = []
Filetarget = []
Filevalue = []

Filelabel.append("EquilibriumPhase")
i = 0
for f in developerDict["files"]:
	filecounter = 0
	for line in f['lines']: 
		#print(line['branches'])
		if line['branches']!= []:
			continue
		if line['count']!=0:
			filecounter += 1
			#print(line['count']) 
		#print(line['gcovr/noncode']) 
		#print(line['line_number']) 
	if filecounter !=0:
		#fileDict[file['file']] = filecounter
		y=f['file'].split('/')
		for t in range(2):
			if y[t] is not None:
				Filelabel.append(y[t])
				Filesource.append(0)
				i += 1
				Filetarget.append(i)
				Filevalue.append(filecounter)
		#print("Test files: ", file['file'])
		#print("File counter: ", filecounter)
#print("Test lines: ", developerDict["files"]["lines"])
#print(fileDict)
print("Done reading nested JSON Array")

fig = go.Figure(data=[go.Sankey(
    node = dict(
      pad = 15,
      thickness = 20,
      line = dict(color = "black", width = 0.5),
      label = Filelabel,
      color = "blue"
    ),
    link = dict(
      source = Filesource, # indices correspond to labels, eg A1, A2, A1, B1, ...
      target = Filetarget,
      value = Filevalue
  ))])

fig.update_layout(title_text="Basic Sankey Diagram", font_size=10)
fig.show()
