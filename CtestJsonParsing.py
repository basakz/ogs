import json

some_ctests = ["SurfaceComplexation", "EquilibriumPhase", "KineticReactant"]

for test in some_ctests:
	cTestFile = open(test + '.json', 'r') 

	print("Started reading nested JSON array")
	developerDict = json.load(cTestFile)
	fileDict = {}

	for file in developerDict["files"]: 
		filecounter = 0	
		for line in file['lines']: 
			#print(line['branches'])
			if line['branches']!= []:
				continue
			if line['count']!=0:
				filecounter += 1
				
			#print(line['gcovr/noncode']) 
			#print(line['line_number']) 
		if filecounter !=0:
			fileDict[file['file']] = filecounter
		
	#print("Test lines: ", developerDict["files"]["lines"])
	print(fileDict)
	print("Done reading nested JSON Array")