import os

isJson = True;
isHtml = True;
some_ctests = ["SurfaceComplexation", "EquilibriumPhase", "KineticReactant"]
report_path = "../coverage_reports/"

# create coverage report main directory
os.system("mkdir -p " + report_path) 

for t in some_ctests:
	# clean coverage counters
    os.system("make clean_coverage")

	# running a specific ctest
    os.system("ctest -R " + t)
    
    if isJson :
        # collect coverage into t.json # for specific test
        os.system("gcovr -r ../ogs --json -o " +t+".json ../build")
        
        # create coverage report subdirectory
        os.system("mkdir -p " + report_path + "/" + t + "/json") 
        
        # move coverage file
        os.system("mv -f " + t + ".json " + report_path + "/" + t + "/json")
        
    if isHtml :
        # collect coverage into t.html # for specific test
        os.system("gcovr -r ../ogs --html-details -o " +t+".html ../build")
        
         # create coverage report subdirectory
        os.system("mkdir -p " + report_path + "/" + t + "/html")
        
        # move coverage file
        os.system("mv -f " + "*.html " + report_path + "/" + t + "/html")