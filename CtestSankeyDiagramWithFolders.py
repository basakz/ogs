import json
import plotly.graph_objects as go

cTestFile = open('EquilibriumPhase.json', 'r') 

print("Started reading nested JSON array")
developerDict = json.load(cTestFile)
#fileDict = {}
Filelabel = []
Filesource = []
Filetarget = []
Filevalue = []

Filelabel.append("EquilibriumPhase")
i = 0
# f parameter is used to find file value in the json
for f in developerDict["files"]:
	filecounter = 0
	for line in f['lines']: 
		#print(line['branches'])
		if line['branches']!= []:
			continue
		if line['count']!=0:
			filecounter += 1
			#print(line['count']) 
		#print(line['gcovr/noncode']) 
		#print(line['line_number']) 
	if filecounter !=0:
		#fileDict[file['file']] = filecounter
		y=f['file'].split('/')
		number_of_elements = len(y)
		#print(number_of_elements)
		j=0
		sourceValue=0 #sourceValue value keeps the stages
		while j < number_of_elements:
			#print(y[j])	
			if y[j] not in Filelabel:
				Filelabel.append(y[j])
				Filesource.append(j)				
				i += 1
				Filetarget.append(i)
				Filevalue.append(filecounter)
				sourceValue= i

				k=j+1
			if k < number_of_elements:
				Filelabel.append(y[k])				
				i += 1
				Filetarget.append(i)
				Filevalue.append(filecounter)
				Filesource.append(sourceValue)
			j += 1
		#print(Filelabel)
			# print(Filesource)
			# print(Filetarget)
			# print(Filevalue)
				
print("Done reading nested JSON Array")

fig = go.Figure(data=[go.Sankey(
    node = dict(
      pad = 15,
      thickness = 20,
      line = dict(color = "black", width = 0.5),
      label = Filelabel,
      color = "blue"
    ),
    link = dict(
      source = Filesource, # indices correspond to labels, eg A1, A2, A1, B1, ...
      target = Filetarget,
      value = Filevalue
  ))])

fig.update_layout(title_text="Basic Sankey Diagram", font_size=10)
fig.show()